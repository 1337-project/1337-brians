/**
 * plugins/vuetify.js
 *
 * Framework documentation: https://vuetifyjs.com`
 */

// Styles
import "vuetify/styles";
import "@mdi/font/css/materialdesignicons.css";

// Composables
import { createVuetify } from "vuetify";

const skulls = {
    dark: true,
    colors: {
        background: "#000000",
        primary: "#48DD00", // Terminal Green
        secondary: "#37AB33", // Darker Green
        terminal: "#72FE7D", // Lighter Green for the actual terminal text
        terminal2: "#48DD00", // (same as primary)
        red: "#ff0000",
        blue: "#32afff",
    },
};

// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
export default createVuetify({
    theme: {
        defaultTheme: "skulls",
        themes: {
            skulls,
        },
    },
    icons: {
        defaultSet: "mdi",
    },
});
