import { createRouter, createWebHistory } from "vue-router";
import HomeLayout from "@/layouts/HomeLayout.vue";
import UploadLayout from "@/layouts/UploadLayout.vue";
import CurateLayout from "@/layouts/CurateLayout.vue";
import SimulateLayout from "@/layouts/SimulateLayout.vue";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: "/",
            name: "home",
            component: HomeLayout,
        },
        {
            path: "/upload",
            name: "upload",
            component: UploadLayout,
        },
        {
            path: "/curate",
            name: "curate",
            component: CurateLayout,
        },
        {
            path: "/simulate",
            name: "simulate",
            component: SimulateLayout,
        },
    ],
});

export default router;
